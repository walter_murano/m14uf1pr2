import { Component } from '@angular/core';
import { Registro } from 'src/app/models/registro.models';
import { DataService } from 'src/app/services/data.service';

@Component({
  selector: 'app-tab2',
  templateUrl: 'tab2.page.html',
  styleUrls: ['tab2.page.scss'],
})
export class Tab2Page {
  constructor(public data: DataService) {}

  sendEmail() {
    console.log('Send Email');
  }

  abrirRegistro(registro: Registro) {
    this.data.abrirRegistro(registro);
  }
}