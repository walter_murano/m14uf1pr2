import { Injectable } from '@angular/core';
import { InAppBrowser } from '@ionic-native/in-app-browser/ngx';
import { NavController } from '@ionic/angular';
import { Registro } from '../models/registro.models';

import { Storage } from '@ionic/storage-angular';
import { File } from '@ionic-native/file/ngx';
import { EmailComposer } from '@ionic-native/email-composer/ngx';

@Injectable({
  providedIn: 'root',
})
export class DataService {
  registers: Registro[] = [];

  constructor(
    private storage: Storage,
    private navController: NavController,
    private inAppBrowser: InAppBrowser,
    private file: File,
    private emailComposer: EmailComposer
  ) {
    this.storage.create();
    this.loadStorage();

  }

  async loadStorage() {
    this.registers = (await this.storage.get('registros')) || [];
  }

  async guardarRegistro(format: string, text: string) {
    await this.loadStorage();
    const newRegister = new Registro(format, text);
    this.registers.unshift(newRegister);

    this.storage.set('registros', this.registers);
  }

  abrirRegistro(register: Registro) {
    const http = 'http';
    const geo = 'geo';
    this.navController.navigateForward('tabs/tab2');

    switch (register.type) {
      case http:
        this.inAppBrowser.create(register.text, '_system');
        break;
      case geo:
        this.navController.navigateForward(`/tabs/tab2/maps/${register.text}`);
        break;
    }
  }

  sendEmail(){
    const temp=[];
    const titles: string ="Tipo,Formato,Creado en , Texto \n";

    temp.push(titles);
    this.registers.forEach(element => {
      const linea =
      `${element.type},${element.format},${element.created},${element.text.replace(',','')}\n`;
      temp.push(linea)
    });

    this.newFisicFile(temp.join(''));
  }
  newFisicFile(text: string) {
    this.file.checkFile(this.file.dataDirectory, 'registers.csv').then(
      exist=> {
        return this.writeInFile(text);
      }
    ).catch(err=> {
      return this.file.createFile(this.file.dataDirectory,'registers.csv',false)
      .then(created=> this.writeInFile(text))
      .catch(err2=> console.log('No se pudo crear el archivo',err2));
    })
  }

  async writeInFile(text: string){
    await this.file.writeExistingFile(this.file.dataDirectory,'registers.csv', text);

    const file = `${this.file.dataDirectory}registers.csv`;
    let email={
      to:`walter.murano.7e3@itb.cat`,
      //cc 'erika@mustermann.de',
      //bcc ['john@doe.com', 'jane@doe.com'],
      attachments:[
        file
      ],
      subject: 'Backup de scans',
      body: 'Aqui tienes la copia de todos los códigos escaneados.<strong>Scan App</strong>',
      isHtml:true

    }

    //send a text message using default options
    this.emailComposer.open(email);
  }



}