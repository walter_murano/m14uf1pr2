export class Registro {
  public format: string;
  public text: string;
  public type: string;
  public icon: string;
  public created: Date;

  constructor(format: string, text: string) {
    this.format = format;
    this.text = text;
    this.created = new Date();

    this.determinarTipo();

  }

  private determinarTipo() {
    const http = 'http';
    const geo = 'geo:';

    const inicioTexto = this.text.substr(0, 4);

    switch (inicioTexto) {
      case http:
        this.type = http;
        this.icon = 'globe';
        break;
      case geo:
        this.type = geo.substr(0, 3);
        this.icon = 'pin';
        break;
      default:
        this.type = 'Unknown';
        this.icon = 'create';
    }
  }
}